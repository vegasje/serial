package VC0706

import (
	"github.com/vegasje/go-serial"
	"log"
	"math"
	"time"
)

const (
	STARTING_BAUD = serial.BAUD_38400

	COMMANDSEND  = 0x56
	COMMANDREPLY = 0x76
	COMMANDEND   = 0x00

	CMD_GETVERSION = 0x11
	CMD_RESET      = 0x26
	CMD_TAKEPHOTO  = 0x36
	CMD_READBUFF   = 0x32
	CMD_GETBUFFLEN = 0x34
	CMD_DOWNSIZE   = 0x54
	CMD_COLOR      = 0x3c
	CMD_SET_PORT   = 0x24

	FBUF_CURRENTFRAME = 0x00
	FBUF_NEXTFRAME    = 0x01

	FBUF_STOPCURRENTFRAME = 0x00
	FBUF_RESUMEFRAME      = 0x03

	SIZE_640x480 = 0x00
	SIZE_320x240 = 0x11
	SIZE_160x120 = 0x22

	COLOR_AUTO  = 0x00
	COLOR_COLOR = 0x01
	COLOR_BW    = 0x02
)

type Device struct {
	Port     string
	SerialNo byte
	Serial   *serial.Connection
	Timeout  time.Duration
}

func New(port string, serialNo byte, timeout time.Duration) (*Device, error) {
	var err error
	device := Device{Port: port, SerialNo: serialNo, Timeout: timeout}

	device.Serial, err = serial.Open(device.Port, STARTING_BAUD, device.Timeout)
	if err != nil {
		log.Fatalf("Unable to initalize serial connection: %v", err)
		return nil, err
	}

	return &device, nil
}

func (d *Device) Write(message []byte) error {
	_, err := d.Serial.Write(message)
	if err != nil {
		log.Fatalf("Unable to write: %v", err)
		return err
	}

	log.Printf("wrote: %v", message)
	return nil
}

func (d *Device) Read(alloc uint) ([]byte, error) {
	buf := make([]byte, alloc)
	n, err := d.Serial.Read(buf)
	if err != nil {
		log.Fatalf("Unable to read: %v", err)
		return nil, err
	}

	log.Printf("read: %v", buf[:n])
	return buf[:n], nil
}

func (d *Device) CheckReply(reply []byte, cmd byte) bool {
	if reply[0] == COMMANDREPLY && reply[1] == d.SerialNo && reply[2] == cmd && reply[3] == 0x00 {
		return true
	}

	return false
}

func (d *Device) Reset() bool {
	log.Println("resetting")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_RESET, COMMANDEND}
	d.Write(cmd)

	// read initial reset response
	reply, err := d.Read(5)
	if err != nil {
		log.Fatalf("Unable to reset: %v", err)
		return false
	}

	if !d.CheckReply(reply, CMD_RESET) {
		return false
	}

	// read device information
	reply, err = d.Read(95)
	if err != nil {
		log.Fatal(err)
		return false
	}

	return true
}

func (d *Device) GetVersion() bool {
	log.Println("getting version")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_GETVERSION, COMMANDEND}
	d.Write(cmd)

	reply, err := d.Read(16)
	if err != nil {
		log.Fatal(err)
		return false
	}

	if d.CheckReply(reply, CMD_GETVERSION) {
		log.Printf("version: %v", string(reply))
		return true
	}

	return false
}

func (d *Device) SetBaudRate(baud serial.Baud) bool {
	log.Println("setting baud rate")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_SET_PORT, 0x02, byte(baud>>8) & 0xff, byte(baud) & 0xff}
	d.Write(cmd)

	/*err := d.Serial.SetBaudRate(baud)
	if err != nil {
		log.Fatal(err)
		return false
	}*/

	d.Serial.Close()
	var err error
	d.Serial, err = serial.Open(d.Port, serial.BAUD_115200, d.Timeout)
	if err != nil {
		log.Fatalf("Unable to initalize serial connection: %v", err)
		return false
	}

	reply, err := d.Read(5)
	if err != nil {
		log.Fatal(err)
		return false
	}

	return d.CheckReply(reply, CMD_SET_PORT)
}

func (d *Device) SetSize(size byte) bool {
	log.Println("setting size")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_DOWNSIZE, 0x01, size}
	d.Write(cmd)

	reply, err := d.Read(5)
	if err != nil {
		log.Fatal(err)
		return false
	}

	return d.CheckReply(reply, CMD_DOWNSIZE)
}

func (d *Device) SetColor(color byte) bool {
	log.Println("setting color")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_COLOR, 0x02, 0x01, color}
	d.Write(cmd)

	reply, err := d.Read(5)

	if err != nil {
		log.Fatal(err)
		return false
	}

	return d.CheckReply(reply, CMD_COLOR)
}

func (d *Device) TakePhoto() bool {
	log.Println("taking photo")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_TAKEPHOTO, 0x01, FBUF_STOPCURRENTFRAME}
	d.Write(cmd)
	reply, err := d.Read(5)

	if err != nil {
		log.Fatal(err)
		return false
	}

	return d.CheckReply(reply, CMD_TAKEPHOTO) && reply[3] == 0x0
}

func (d *Device) ResumeFeed() bool {
	log.Println("resuming feed")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_TAKEPHOTO, 0x01, FBUF_RESUMEFRAME}
	d.Write(cmd)
	reply, err := d.Read(5)

	if err != nil {
		log.Fatal(err)
		return false
	}

	return d.CheckReply(reply, CMD_TAKEPHOTO) && reply[3] == 0x0
}

func (d *Device) GetBufferLength() uint {
	log.Println("getting buffer length")

	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_GETBUFFLEN, 0x01, FBUF_CURRENTFRAME}
	d.Write(cmd)
	reply, err := d.Read(9)

	if err != nil {
		log.Fatal(err)
		return 0
	}

	if d.CheckReply(reply, CMD_GETBUFFLEN) && reply[4] == 0x4 {
		l := uint(reply[5])
		l <<= 8
		l += uint(reply[6])
		l <<= 8
		l += uint(reply[7])
		l <<= 8
		l += uint(reply[8])
		return l
	}

	return 0
}

func (d *Device) ReadBuffer(bytes uint) []byte {
	cmd := []byte{COMMANDSEND, d.SerialNo, CMD_READBUFF, 0x0c, FBUF_CURRENTFRAME, 0x0a}
	photo := []byte{}

	// the initial offset into the frame buffer
	var addr uint = 0x0

	// bytes to read each time (must be a mutiple of 4)
	inc := 8192

	for uint(addr) < bytes {
		// on the last read, we may need to read fewer bytes
		chunk := uint(math.Min(float64(bytes-uint(addr)), float64(inc)))

		// append 4 bytes that specify the offset into the frame buffer
		command := append(cmd, []byte{byte(addr>>24) & 0xff,
			byte(addr>>16) & 0xff,
			byte(addr>>8) & 0xff,
			byte(addr) & 0xff}...)

		// append 4 bytes that specify the data length to read
		command = append(command, []byte{byte(chunk>>24) & 0xff,
			byte(chunk>>16) & 0xff,
			byte(chunk>>8) & 0xff,
			byte(chunk) & 0xff}...)

		// append the delay
		command = append(command, []byte{1, 0}...)

		log.Printf("Reading %v bytes at %v", chunk, addr)
		d.Write(command)

		// the reply is a 5-byte header, followed by the image data,
		// followed by the 5-byte header again
		reply, err := d.Read(5 + chunk + 5)
		if err != nil {
			log.Fatal(err)
			return nil
		}

		// verify that the response length was what we expected
		replyLen := uint(len(reply))
		expectedLen := 5 + chunk + 5
		if replyLen != expectedLen {
			// retry the read if we didn't get enough bytes back.
			log.Printf("Read %v but expected %v. Retrying", replyLen, expectedLen)
			continue
		}

		if !d.CheckReply(reply, CMD_READBUFF) {
			log.Fatal("ERROR READING PHOTO")
			return nil
		}

		// append the data between the header data to photo
		photo = append(photo, reply[5:(chunk+5)]...)

		// advance the offset into the frame buffer
		addr += chunk
	}

	log.Printf("%v bytes written", addr)

	return photo
}
